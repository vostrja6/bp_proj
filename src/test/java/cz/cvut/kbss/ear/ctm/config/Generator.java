//package cz.cvut.kbss.ear.ctm.config;
//
//import cz.cvut.kbss.ear.ctm.model.Address;
//import cz.cvut.kbss.ear.ctm.model.Doctor;
//import cz.cvut.kbss.ear.ctm.model.Person;
//import cz.cvut.kbss.ear.ctm.model.enums.HealthCareInsurer;
//
//import java.util.Random;
//
//public class Generator
//{
//
//    public static final Random RANDOM = new Random();
//
//    private Generator()
//    {
//        throw new AssertionError();
//    }
//
//    public static Doctor generateDoctor()
//    {
//        return new Doctor(
//                "Tomáš",
//                "Hrabáček",
//                "test@fel.cvut.cz",
//                "739123676",
//                "123",
//                "123456798CRN",
//                "www.docotr.cz",
//                "Description......");
//    }
//
//    public static Person generatePatient1()
//    {
//        return new Person(
//                "Tomáš",
//                "Hrabáček",
//                "hrabato2@fel.cvut.cz",
//                "739123676",
//                "9702172558", "123",
//                HealthCareInsurer.VZP,
//                new Address("Žatec", "Husova", "2081", "43801"));
//    }
//
//    public static Person generatePatient2()
//    {
//        return new Person("Pavel",
//                "Pek",
//                "pekpavel@fel.cvut.cz",
//                "11122333",
//                "9609172639", "456",
//                HealthCareInsurer.VZP,
//                new Address("Velichov",
//                        "Velichov",
//                        "12",
//                        "43801"));
//    }
//
//
//}
