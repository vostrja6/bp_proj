'use strict';

import Reflux from "reflux";
import Actions from "../actions/Actions";
import Ajax from "../utils/Ajax";

const BASE_URL = 'rest/rooms';
const BASE_URL_WITH_SLASH = 'rest/rooms/';

// When rooms are being loaded, do not send the request again
let rooms = false;
let reservations = false;

export default class RoomStore extends Reflux.Store {

    constructor() {
        super();
        this.listenables = Actions;
    }

    onLoadAllRooms(onSuccess) {
        if (rooms) {
            return;
        }
        rooms = true;
        Ajax.get(BASE_URL).end((data) => {
            rooms = false;
            this.setState({rooms: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            rooms = false;
            this.setState({rooms: []});
        });
    }

    onLoadRoom(id, onFinish) {
        Ajax.get(BASE_URL_WITH_SLASH + 'findById=' + id).end((data) => {
            this.setState({room: data});
            if (onFinish) {
                onFinish();
            }
        }, () => {
            this.setState({room: null});
            if (onFinish) {
                onFinish();
            }
        });
    }

    onLoadRoomDayReservations(roomId, date, onSuccess) {
        console.log("sending request for dayRes where id=" + roomId + " and date=" + date);
        Ajax.get(BASE_URL_WITH_SLASH + 'findById=' + roomId + '/date=' + date).end((data) => {
            this.setState({reservations: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            reservations = false;
            this.setState({reservations: []});
        });
    }
}
