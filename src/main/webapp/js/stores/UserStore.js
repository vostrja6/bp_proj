'use strict';

import Reflux from "reflux";
import Actions from "../actions/Actions";
import Ajax from "../utils/Ajax";

export default class UserStore extends Reflux.Store {

    constructor() {
        super();
        this.state = {user: null, loaded: false};
        this.listenables = Actions;
    }

    onLoadUser() {
        Ajax.get('rest/persons/current').end((user) => {
            this.setState({user: user, loaded: true});
        }, () => {
            this.setState({loaded: true});
        });
    }

    onUpdateUser(user, onSuccess, onError) {
        Ajax.put('rest/persons/current', user).end(onSuccess, onError);
    }
}
