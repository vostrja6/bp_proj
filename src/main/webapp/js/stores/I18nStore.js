'use strict';

/**
 * Internationalization store for access from non-react components and objects.
 */
class I18nStore {
    constructor() {
        this.messages = [];
    }

    setMessages(messages) {
        this.messages = messages;
    }

    i18n(messageId) {
        return this.messages[messageId];
    }
}

const INSTANCE = new I18nStore();

export default INSTANCE;
