'use strict';

import React from "react";
import {Alert, Button, Panel, Grid} from "react-bootstrap";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Logger from "../../utils/Logger";
import Input from "../HorizontalInput";
import Mask from "../Mask";
import PropTypes from "prop-types";
import Ajax from "../../utils/Ajax";
import Select from "react-select";
import PersonValidator from "../../validator/PersonValidator";

class Reservation extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;

        this.state = {
            date: '',
            timeFrom: '',
            personId: this.props.user.id,
            status: 0,
            roomId: '',
            roomOptions: [],
            resExists: false,
        };
    }

    handleChangeRoom = (id) => {
        this.setState({roomId: id});
    };

    _setRooms = () => {
        let roomOptions = [];
        if (!(this.state.roomOptions.length > 0)) {
            for (let i = 0; i < this.props.rooms.length; i++) {
                roomOptions[i] = {
                    label: this.props.rooms[i].name,
                    id: this.props.rooms[i].id
                };
            }
        }
        this.state.roomOptions = roomOptions;
    };

    _onChange = (e) => {
        const change = {};
        change[e.target.name] = e.target.value;
        this.setState(change);
    };

    createReservation = () => {

        var time_hours = parseInt(this.state.timeFrom.substring(0, 2));
        var time_min = parseInt(this.state.timeFrom.substring(3, 5));
        var timeTo_min = time_min + 30;
        var timeTo_hours = time_hours;

        //if time to = 60 !!!!!!!!!
        //transfer to hours
        if (timeTo_min === 60) {
            timeTo_min = '00';
            timeTo_hours = time_hours + 1;
        }

        var timeTo = timeTo_hours + ':' + timeTo_min;
        console.log('Time to: ' + timeTo);


        const reservationData = {
            date: this.state.date,
            timeFrom: this.state.timeFrom,
            timeTo: timeTo,
            status: this.state.status,
            personId: this.props.user.id,
            roomId: this.state.roomId.id
        };

        this.props.actions.createReservation(reservationData);
    };

    _findReservation = (date, time, roomId) => {

        this.props.actions.findReservation(date, time, roomId.id);
        console.log("Result on front: " + this.props.reservation);
        if (this.props.reservation === null) {
            this.state.resExists = false;
        }
        else {
            this.state.resExists = true;
        }

        if (!this.state.resExists) {
            return;
        }

        if (this.state.resExists === false) {
            this.props.showSuccessMessage('Reservation not exists. You can create one.')
        }
        else {
            this.props.showErrorMessage('Reservation already exists. You can see rooms reservations for more info.')
        }

    };

    validateResData = (date, time, roomId) => {
        const dateRegex = /(?<Day>[0-2][0-9]|[3][0-1])-(?<Month>1[0-2]|0[1-9])-(?<Year>19|2[0-9])/;
        const timeRegex = /(?<Hours>09|1[0-7]):(?<Minutes>00|30)/;

        var date_now = new Date();

        var day = parseInt(this.state.date.substring(0, 2));
        var month = parseInt(this.state.date.substring(3, 5));
        var year = parseInt(this.state.date.substring(6, 8));

        var date_res = new Date(year, (month-1), date);

        var diff = parseInt((date_res-date_now)/(1000*60));

        console.log(diff);

        const datePass = this.state.date.length === 8 && this.state.date.match(dateRegex);
        const timePass = this.state.timeFrom.length === 5 && this.state.timeFrom.match(timeRegex);

        if (!date.length === 8 || !datePass || !time.length === 5 || !timePass || !roomId) {
            return false;
        }
        return true;
    };

    render() {
        if (!this.props.rooms) {
            return <Mask text='Loading'/>;
        }
        if (this.state.roomOptions.length === 0) {
            this._setRooms();
        }

        const dateRegex = /(?<Day>[0-2][0-9]|[3][0-1])-(?<Month>1[0-2]|0[1-9])-(?<Year>19|2[0-9])/;
        const timeRegex = /(?<Hours>09|1[0-7]):(?<Minutes>00|30)/;

        const datePass = this.state.date.length === 8 && this.state.date.match(dateRegex);
        const timePass = this.state.timeFrom.length === 5 && this.state.timeFrom.match(timeRegex);

        return <Panel header={<h3>Reservation form</h3>}>
            <form className='form-horizontal' style={{margin: '0.5em 0 0 0'}}>
            <div className='row'>
                <div className='col-xs-3'>
                    <b>Time *</b>
                    <Input
                        type='text' name='timeFrom'
                        value={this.state.timeFrom}
                        placeholder={'HH:MM'}
                        labelWidth={15} onChange={this._onChange}
                        validation={!timePass && this.state.timeFrom.length > 0 ? 'error' : null}
                        title={!timePass ? 'Put time in 30minute interval matching format: HH:MM (09:00-17:30)' : null}
                    />
                    <span className="error text-danger pull-right">
                        {(!timePass && this.state.timeFrom.length === 5) ? 'Put time in format: HH:MM (09:00-17:30)' : null}
                    </span>
                </div>
            </div>

            <div className='row'>
                <div className='col-xs-3'>
                    <b>Date *</b>
                    <Input
                        type='text' name='date'
                        value={this.state.date}
                        placeholder={'DD-MM-YY'}
                        labelWidth={15} onChange={this._onChange}
                        validation={!datePass && this.state.date.length > 0 ? 'error' : null}
                        title={!datePass ? 'Put date in format: DD-MM-YY' : null}
                    />
                    <span className="error text-danger pull-right">
                        {(!datePass && this.state.date.length === 8) ? 'Put date in format: DD-MM-YY' : null}
                    </span>
                </div>
            </div>

            <div className='row'>
                <div className='col-xs-3'>
                    <b>Room *</b>
                    <Select
                        name='roomId'
                        value={this.state.roomId}
                        options={this.state.roomOptions}
                        inputWidth={3}
                        placeholder={'Room'}
                        onChange={this.handleChangeRoom}
                    />
                    <span className="error text-danger pull-right">
                        {(this.state.roomId > 0) ? 'Choose a room' : null}
                    </span>
                </div>
            </div>
            <div className='row'>

            </div>
            <div style={{margin: '2em 1em 1em 1em', textAlign: 'left'}}>
                <Button
                    bsStyle='danger' bsSize='small'
                    onClick={() => this.props.actions.findReservation(this.state.date, this.state.timeFrom, this.state.roomId.id)} style={{margin: '0 0 0 3.2em'}}
                    disabled={this.state.mask || !datePass || !timePass || !this.state.roomId || !this.validateResData(this.state.date, this.state.timeFrom, this.state.roomId)}
                >
                    {'Find reservation'}
                </Button>
                {'                '}
                <Button
                    bsStyle='success' bsSize='small'
                    disabled={this.state.mask || !datePass || !timePass || !this.state.roomId || !this.props.notFound}
                    onClick={() => this.createReservation()}
                >
                    {'Create reservation'}
                </Button>
            </div>
            </form>
        </Panel>;
    }
}

Reservation.propTypes = {
    actions: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    rooms: PropTypes.array,
    reservation: PropTypes.object,
    notFound: PropTypes.bool.isRequired
};

export default injectIntl(I18nWrapper(Reservation));
