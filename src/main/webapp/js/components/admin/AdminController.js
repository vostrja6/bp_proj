'use strict';

import React from "react";
import Reflux from "reflux";
import Actions from "../../actions/Actions";

import UserStore from "../../stores/UserStore";
import DoctorStore from "../../stores/DoctorStore";

import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import MessageWrapper from "../misc/MessageWrapper";

import AddDoctor from "./AddDoctor";

import Routes from "../../utils/Routes";
import Routing from "../../utils/Routing";
import RoutingRules from "../../utils/RoutingRules";

class AdminController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.store = [DoctorStore, UserStore];
        this.storeKeys = ['doctors', 'user'];
    }

    componentWillMount() {
        this.setState({loading: true});
    }

    _onSuccees = () =>
    {
        this.setState({loading: false});
    };

    onAddDoctor = (doctorId) => {
        Actions.addDoctor(doctorId, () => this.props.showSuccessMessage('Doktor úspěšně přidán.'));
    };

    render() {
        const actions = {
            addDoctor: this.onAddDoctor
        };

        return <AddDoctor
            actions={actions}
        />;
    }
}

export default injectIntl(I18nWrapper(MessageWrapper(AdminController)));