'use strict';

import React from "react";
import PropTypes from "prop-types";
import {ClipLoader} from "react-spinners";
import I18Store from "../stores/I18nStore";

const Mask = (props) => {
    const text = props.text ? props.text : I18Store.i18n('please-wait');
    return (
        <div className={props.classes ? props.classes : 'mask'}>
            <div className='spinner-container'>
                <div style={{width: 32, height: 32, margin: 'auto'}}>
                    <ClipLoader color='#337ab7' size={32}/>
                </div>
                <div className='spinner-message'>{text}</div>
            </div>
        </div>
    );
};

Mask.propTypes = {
    text: PropTypes.string,
    classes: PropTypes.string
};

export default Mask;
