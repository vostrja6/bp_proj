'use strict';

import React from "react";
import Reflux from "reflux";
import {MenuItem, Nav, Navbar, NavBrand, NavDropdown, NavItem} from "react-bootstrap";
import {Route, Switch} from "react-router-dom";
import {LinkContainer} from "react-router-bootstrap";
import {IfGranted} from "react-authorization"
import Actions from "../actions/Actions";
import Authentication from "../utils/Authentication";
import Constants from "../constants/Constants";
import DashboardController from "./dashboard/DashboardController";
import I18nWrapper from "../i18n/I18nWrapper";
import injectIntl from "../utils/injectIntl";
import Mask from "./Mask";
import ProfileController from "./profile/ProfileController";
import Routes from "../utils/Routes";
import UserStore from "../stores/UserStore";
import RoomsController from "./rooms/RoomsController";
import MyReservationsController from "./reservation/MyReservationsController";
import RoomController from "./rooms/RoomController";
import ReservationController from "./reservation/ReservationController";

class MainView extends Reflux.Component {
    constructor(props) {
        super(props);
        this.state = {showProfile: false};
        this.store = UserStore;
        this.i18n = props.i18n;
    }

    componentWillMount() {
        super.componentWillMount();
        Actions.loadUser();
    }

    openUserProfile = () => {
        this.setState({showProfile: true});
    };

    closeUserProfile = () => {
        this.setState({showProfile: false});
    };

    render() {
        if (!this.state.loaded) {
            return <Mask/>;
        }
        if (this.state.user === null) {
            return (<div>{this.props.children}</div>);
        }
        const name = this.state.user.firstName.substr(0, 1) + '. ' + this.state.user.lastName;
        return <div>
            <header>
                <Navbar fluid={true}>
                    <Navbar.Header>
                        <Navbar.Brand>{Constants.APP_NAME}</Navbar.Brand>
                    </Navbar.Header>
                    <IfGranted expected='USER' actual={this.state.user.role}>
                        <Nav>
                            <LinkContainer
                                to={Routes.dashboard.path}><NavItem>Dashboard</NavItem>
                            </LinkContainer>
                            <LinkContainer
                                to={Routes.reservation.path}><NavItem>Create reservation</NavItem>
                            </LinkContainer>
                            <LinkContainer
                                to={Routes.myReservations.path}><NavItem>My reservations</NavItem>
                            </LinkContainer>
                            <LinkContainer
                                to={Routes.rooms.path}><NavItem>Rooms</NavItem>
                            </LinkContainer>
                        </Nav>
                        <Nav pullRight style={{margin: '0 -15px 0 0'}}>
                            <NavDropdown id='logout' title={name}>
                                <MenuItem onClick={this.openUserProfile}>{this.i18n('main.user-profile')}</MenuItem>
                                <MenuItem href='#' onClick={Authentication.logout}>{this.i18n('main.logout')}</MenuItem>
                            </NavDropdown>
                        </Nav>
                    </IfGranted>
                    <IfGranted expected='ADMIN' actual={this.state.user.role}>
                        <Nav>

                        </Nav>
                        <Nav pullRight style={{margin: '0 -15px 0 0'}}>
                            <NavDropdown id='logout' title={name}>
                                <MenuItem onClick={this.openUserProfile}>{this.i18n('main.user-profile')}</MenuItem>
                                <MenuItem href='#' onClick={Authentication.logout}>{this.i18n('main.logout')}</MenuItem>
                            </NavDropdown>
                        </Nav>
                    </IfGranted>
                </Navbar>
            </header>
            <section style={{height: '100%'}}>
                <ProfileController show={this.state.showProfile} onClose={this.closeUserProfile}/>
                <Switch>
                    <Route path={Routes.dashboard.path} component={DashboardController}/>
                    <Route path={Routes.rooms.path} component={RoomsController}/>
                    <Route path={Routes.room.path} component={RoomController}/>
                    <Route path={Routes.myReservations.path} component={MyReservationsController}/>
                    <Route path={Routes.reservation.path} component={ReservationController}/>
                </Switch>
            </section>
        </div>;
    }
}

export default injectIntl(I18nWrapper(MainView));
