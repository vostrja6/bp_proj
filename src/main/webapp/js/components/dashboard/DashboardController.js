'use strict';

import React from "react";
import Reflux from "reflux";

import Dashboard from "./Dashboard";
import Routes from "../../utils/Routes";
import Routing from "../../utils/Routing";
import RoutingRules from "../../utils/RoutingRules";
import UserStore from "../../stores/UserStore";

export default class DashboardController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.store = UserStore;
    }

    componentWillMount() {
        super.componentWillMount();
        RoutingRules.execute(Routes.dashboard.name);
    }

    createEmptyReport() {
        Routing.transitionTo(Routes.createReport);
    }

    openReport(report) {
        Routing.transitionTo(Routes.editReport, {
            params: {reportId: report.id}
        });
    }

    showReports() {
        Routing.transitionTo(Routes.reports);
    }

    render() {
        return <div>
            <Dashboard
                userFirstName={this.state.user.firstName}
                showAllReports={this.showReports}
                createEmptyReport={this.createEmptyReport}
                openReport={this.openReport}
            />
        </div>;
    }
}
