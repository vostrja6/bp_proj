'use strict';

import React from "react";
import assign from "object-assign";
import Constants from "../../constants/Constants";
import getDisplayName from "../../utils/getDisplayName";

function sortStateTransition(current) {
    switch (current) {
        case Constants.SORTING.NO:
            return Constants.SORTING.DESC;
        case Constants.SORTING.DESC:
            return Constants.SORTING.ASC;
        default:
            return Constants.SORTING.NO;
    }
}

function getSorter(sort) {
    const sortSpec = [];
    if (sort) {
        const sortAttributes = Object.getOwnPropertyNames(sort);
        for (let i = 0, len = sortAttributes.length; i < len; i++) {
            const key = sortAttributes[i];
            if (sort[key] !== Constants.SORTING.NO) {
                sortSpec.push({
                    prop: key,
                    desc: sort[key] === Constants.SORTING.DESC
                });
            }
        }
    }
    return sortSpec.length > 0 ? sortFactory(sortSpec) : function () {
        return 0;
    };
}

function sortFactory(sortSpec) {
    return function (a, b) {
        let prop, res = 0;
        for (let i = 0, len = sortSpec.length; i < len; i++) {
            prop = sortSpec[i].prop;
            // This handles property paths such as 'occurrence.name'
            const path = prop.split('.'),
                len = path.length;
            for (let i = 0; i < len - 1; i++) {
                a = a[path[i]];
                b = b[path[i]];
            }
            prop = path[len - 1];
            if (typeof a[prop] === 'string') {
                res = a[prop].localeCompare(b[prop]);
            } else {
                if (a[prop] === undefined) {
                    if (b[prop] === undefined) {
                        res = 0;
                    } else {
                        res = -1;
                    }
                } else if (b[prop] === undefined) {
                    res = 1;
                } else {
                    res = a[prop] > b[prop] ? 1 : (a[prop] === b[prop] ? 0 : -1);
                }
            }
            if (sortSpec[i].desc) {
                res *= -1;
            }
            if (res !== 0) {
                return res;
            }
        }
        return res;
    }
}

/**
 * Adds sorting abilities to the wrapped component.
 * @param Component The wrapped component
 * @param options Configurations. Most importantly, 'sortingAttributes' specify which attributes are used for sorting.
 */
const SortingWrapper = (Component, options) => {
    class Wrapped extends React.Component {

        constructor(props) {
            super(props);
            const sort = {};
            if (options.sortAttributes) {
                options.sortAttributes.forEach(sa => sort[sa] = Constants.SORTING.NO);
            }
            this.state = {
                sort: sort
            };
        }

        getWrappedComponent() {
            // Enable composition of multiple HOCs.
            return this._wrappedComponent && this._wrappedComponent.getWrappedComponent ?
                this._wrappedComponent.getWrappedComponent() : this._wrappedComponent;
        }

        onSort = (column) => {
            const change = {};
            change[column] = sortStateTransition(this.state.sort[column]);
            const newSort = assign(this.state.sort, change);
            this.setState({sort: newSort});
        };

        render() {
            const sorterFunc = getSorter(this.state.sort);
            return <Component ref={(c) => this._wrappedComponent = c} onSort={this.onSort} sortState={this.state.sort}
                              sorter={sorterFunc ? sorterFunc : () => {
                              }} {...this.props}/>;
        }
    }

    Wrapped.displayName = "SortingWrapper(" + getDisplayName(Component) + ")";
    Wrapped.WrappedComponent = Component.WrappedComponent ? Component.WrappedComponent : Component;
    return Wrapped;
};

export default SortingWrapper;
