'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions([
    'loadUser', 'updateUser',

    'loadAllReservations', 'createReservation', 'findReservation',

    'loadAllRooms', 'loadRoomReservation', 'loadRoom', 'loadRoomDayReservations',

    'loadMyReservations', 'removeReservation', 'setConfirmReservation', 'updateReservation', 'setCreatedReservation', 'setWaitingReservation', 'setPassedReservation'
]);

export default Actions;
