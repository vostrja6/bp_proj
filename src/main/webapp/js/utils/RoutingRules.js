'use strict';

const rules = {};


/**
 * Defines rules executed during routing transformation.
 *
 * I.e. when the application transitions from one route to another, a rule can specify additional behaviour for the
 * application.
 * @type {{execute: module.exports.execute}}
 */
export default class RoutingRules {
    /**
     * Executes rules defined for the specified route name.
     * @param routeName Route name
     */
    static execute(routeName) {
        if (rules[routeName]) {
            rules[routeName].forEach((item) => {
                item.call();
            });
        }
    }
};
