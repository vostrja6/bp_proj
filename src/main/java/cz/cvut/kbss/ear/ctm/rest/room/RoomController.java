package cz.cvut.kbss.ear.ctm.rest.room;


import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.Room;
import cz.cvut.kbss.ear.ctm.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@PreAuthorize("permitAll()")
@RestController
@RequestMapping("/rooms")
public class RoomController {

    private RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Room> getRooms() {
        return roomService.getRooms();
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/findById={id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Room findRoomById(@PathVariable("id") Integer id)
    {
        return roomService.getRoom(id);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/findById={id}/date={date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> findDayRoomReservations(@PathVariable("id") Integer id, @PathVariable("date") String date)
    {
        System.out.println("getDayRoomReservations param id: " + id + " date: " + date);
        return roomService.getDayRoomReservations(id, date);
    }
}
