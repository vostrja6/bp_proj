package cz.cvut.kbss.ear.ctm.model.enums;

import lombok.Getter;

@Getter
public enum Role {
    ADMIN,
    USER;
}