package cz.cvut.kbss.ear.ctm.dto;

import cz.cvut.kbss.ear.ctm.model.Person;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto extends Person
{
    private String passwordOriginal;

    public String getPasswordOriginal() {
        return passwordOriginal;
    }

    public void setPasswordOriginal(String passwordOriginal) {
        this.passwordOriginal = passwordOriginal;
    }
}
