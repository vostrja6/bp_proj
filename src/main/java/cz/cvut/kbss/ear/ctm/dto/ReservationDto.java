package cz.cvut.kbss.ear.ctm.dto;

import cz.cvut.kbss.ear.ctm.model.Reservation;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReservationDto extends Reservation
{
    public String date;

    public String timeFrom;

    public String timeTo;

    public Integer status;

    public Integer roomId;

    public Integer personId;
}
