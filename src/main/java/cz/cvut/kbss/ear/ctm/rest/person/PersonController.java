package cz.cvut.kbss.ear.ctm.rest.person;


import cz.cvut.kbss.ear.ctm.dto.PersonDto;
import cz.cvut.kbss.ear.ctm.exception.AlreadyAddedReservationException;
import cz.cvut.kbss.ear.ctm.exception.AlreadyRemovedReservationException;
import cz.cvut.kbss.ear.ctm.exception.NotFoundException;
import cz.cvut.kbss.ear.ctm.model.Person;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.Room;
import cz.cvut.kbss.ear.ctm.rest.AbstractController;
import cz.cvut.kbss.ear.ctm.rest.mapper.DtoMapper;
import cz.cvut.kbss.ear.ctm.rest.util.RestUtils;
import cz.cvut.kbss.ear.ctm.service.PersonService;
import cz.cvut.kbss.ear.ctm.service.ReservationService;
import cz.cvut.kbss.ear.ctm.service.RoomService;
import cz.cvut.kbss.ear.ctm.service.security.SecurityUtils;
import cz.cvut.kbss.ear.ctm.service.security.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@PreAuthorize("permitAll()")
@RestController
@RequestMapping("/persons")
public class PersonController extends AbstractController {

    private PersonService personService;

    private SecurityUtils securityUtils;

    private DtoMapper dtoMapper;

    private final UserDetailsService userDetailsService;

    @Autowired
    public PersonController(PersonService personService, SecurityUtils securityUtils, DtoMapper dtoMapper, UserDetailsService userDetailsService) {
        this.personService = personService;
        this.securityUtils = securityUtils;
        this.dtoMapper = dtoMapper;
        this.userDetailsService = userDetailsService;
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createPerson(@RequestBody Person person) {
        personService.createPerson(person);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Patient {} {} created.", person.getFirstName(), person.getLastName());
        }

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", person.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getCurrentUser() {
        Person a = securityUtils.getCurrentUser();
        return securityUtils.getCurrentUser();
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/current", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCurrentPerson(@RequestBody PersonDto personDto) {
        final Person person = dtoMapper.personDtoToPerson(personDto);
        personService.updatePerson(person, personDto.getPasswordOriginal());
        if (LOG.isDebugEnabled()) {
            LOG.debug("Patient {} successfully updated.", personDto);
        }
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/existsBirthNumber", method = RequestMethod.GET)
    @ResponseBody
    public String birthNumberExist(@RequestParam(name = "birthNumber") String birthNumber) {
        return Boolean.toString(personService.findByBirthNumber(birthNumber) != null);
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(value = "/exists", method = RequestMethod.GET)
    @ResponseBody
    public String usernameExist(@RequestParam(name = "username") String username) {
        boolean found = false;
        try {
            UserDetails person = userDetailsService.loadUserByUsername(username);
            if(person != null)
            {
                found = true;
            }
        }
        catch (UsernameNotFoundException ex) {
            found = false;
        }

        return Boolean.toString(found);
    }
}
