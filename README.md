*IN PROGRESS :construction_worker:*

# Clinical Trial Manager

The main purpose for the application is implement environment for doctors and their patients.  

## Getting Started

### Prerequisites

What things you need to install to use the software and how to install them

```
Git
Maven
Tomcat Apache
Node.JS 
```

### Installing

A step by step series of examples that tell you have to get a development env running


```
$ cd src/main/webapp
$ npm run build
$ npm start
```

Run your Tomcat Apache server:
(In case you use IntelliJ IDEA)

Edit Configurations -> Add Tomcat Server Local

![1.](https://i.imgur.com/udqtmiL.png)

Add Artifact: *ctmwar_exploded*.

![2.](https://i.imgur.com/DUCFfXu.png)


## Deployment

Add additional notes on how to deploy this to production.

## Built With

* [Bootstrap 3](https://getbootstrap.com/docs/3.3/) - Bootstrap 3

## Authors

See also the list of [contributors](https://gitlab.fel.cvut.cz/dlouhka1/NSS-TermProject/wikis/home) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
